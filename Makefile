TEX=xelatex
PDFS=red.pdf

all: 	$(PDFS)

deploy:
	mkdir -p reports 
	cp $(PDFS) reports

red.pdf: red.tex r7rs-large.sty red0.tex
	$(TEX) red.tex
	makeindex -s r7rs-large.ist red.idx
	$(TEX) red.tex

.PHONY: clean
clean:
	rm -f *.out *.aux *.log *.pdf *.toc *.idx *.ilg *.ind
